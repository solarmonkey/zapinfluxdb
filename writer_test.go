package zapinfluxdb

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"testing"
	"time"
)

func TestWriter_Write(t *testing.T) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:     "time",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
		LineEnding:  zapcore.DefaultLineEnding,
		EncodeLevel: zapcore.LowercaseLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
		},
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	lvl := zap.NewAtomicLevelAt(zap.InfoLevel)

	cfg := Config{
		URL:         "http://localhost:8086",
		AuthToken:   "",
		Org:         "",
		Bucket:      "dev/autogen",
		Measurement: "writerlog",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
	}
	writer := NewWriter(cfg)
	core := zapcore.NewCore(zapcore.NewJSONEncoder(encoderConfig),
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), writer), lvl)
	l := zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1))

	l.Info("abc", zap.String("a", "b"), zap.Binary("v", []byte("123456")), zap.Float32("c", 112.3))
	_ = writer.Sync()
}
