package zapinfluxdb

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"runtime"
	"testing"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

func Test_WriteToInfluxDB(t *testing.T) {
	client := influxdb2.NewClient("http://localhost:8086", "")
	writer := client.WriteAPIBlocking("", "dev/autogen")
	p := influxdb2.NewPoint("log", map[string]string{"level": "info"},
		map[string]interface{}{"content": "aaaaaaaa"}, time.Now())
	err := writer.WritePoint(context.Background(), p)
	if err != nil {
		t.Error(err)
	}
}

func TestCore_Write(t *testing.T) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:     "time",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
		LineEnding:  zapcore.DefaultLineEnding,
		EncodeLevel: zapcore.LowercaseLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
		},
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	lvl := zap.NewAtomicLevelAt(zap.InfoLevel)

	cfg := Config{
		URL:         "http://localhost:8086",
		AuthToken:   "",
		Org:         "",
		Bucket:      "dev/autogen",
		Measurement: "suplog",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
	}

	core := NewCore(zapcore.NewJSONEncoder(encoderConfig),
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), lvl, cfg)
	l := zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1))

	l.Info("abc", zap.String("a", "b"), zap.Binary("v", []byte("123456")), zap.Float32("c", 112.3))
	_ = core.Sync()
}

func TestCore_Close(t *testing.T) {
	if true {
		encoderConfig := zapcore.EncoderConfig{
			TimeKey:     "time",
			LevelKey:    "level",
			CallerKey:   "caller",
			MessageKey:  "content",
			LineEnding:  zapcore.DefaultLineEnding,
			EncodeLevel: zapcore.LowercaseLevelEncoder,
			EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
				enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
			},
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
		lvl := zap.NewAtomicLevelAt(zap.InfoLevel)

		cfg := Config{
			URL:         "http://localhost:8086",
			AuthToken:   "",
			Org:         "",
			Bucket:      "dev/autogen",
			Measurement: "suplog",
			LevelKey:    "level",
			CallerKey:   "caller",
			MessageKey:  "content",
		}

		core := NewCore(zapcore.NewJSONEncoder(encoderConfig),
			zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), lvl, cfg)

		fmt.Println(core)
	}
	runtime.GC()
}

func TestCore_Health(t *testing.T) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:     "time",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
		LineEnding:  zapcore.DefaultLineEnding,
		EncodeLevel: zapcore.LowercaseLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
		},
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	lvl := zap.NewAtomicLevelAt(zap.InfoLevel)

	cfg := Config{
		URL:         "http://localhost:8086",
		AuthToken:   "",
		Org:         "",
		Bucket:      "dev/autogen",
		Measurement: "suplog",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
	}

	core := NewCore(zapcore.NewJSONEncoder(encoderConfig),
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), lvl, cfg)

	t.Log(core.Health(time.Now().Add(time.Second * 3)))
}

func TestCore_Ping(t *testing.T) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:     "time",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
		LineEnding:  zapcore.DefaultLineEnding,
		EncodeLevel: zapcore.LowercaseLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
		},
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	lvl := zap.NewAtomicLevelAt(zap.InfoLevel)

	cfg := Config{
		URL:         "http://localhost:8086",
		AuthToken:   "",
		Org:         "",
		Bucket:      "dev/autogen",
		Measurement: "suplog",
		LevelKey:    "level",
		CallerKey:   "caller",
		MessageKey:  "content",
	}

	core := NewCore(zapcore.NewJSONEncoder(encoderConfig),
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), lvl, cfg)

	t.Log(core.Ping())
}
