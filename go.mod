module bitbucket.org/solarmonkey/zapinfluxdb

go 1.15

require (
	github.com/influxdata/influxdb-client-go/v2 v2.2.0
	go.uber.org/zap v1.16.0
)
