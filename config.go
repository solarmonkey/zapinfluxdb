package zapinfluxdb

import influxdb2 "github.com/influxdata/influxdb-client-go/v2"

// Config of influxdb
type Config struct {
	URL         string
	AuthToken   string
	Org         string
	Bucket      string
	Measurement string
	Options     *influxdb2.Options

	LevelKey   string
	CallerKey  string
	MessageKey string
}
