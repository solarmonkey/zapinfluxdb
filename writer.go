package zapinfluxdb

import (
	"context"
	"fmt"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"runtime"
	"time"
)

// Writer wrapper
type Writer struct {
	client influxdb2.Client
	writer api.WriteAPI
	config Config
}

// NewWriter constructor
func NewWriter(config Config) *Writer {
	if config.Options == nil {
		config.Options = influxdb2.DefaultOptions()
	}
	w := &Writer{
		config: config,
	}
	w.client = influxdb2.NewClientWithOptions(config.URL, config.AuthToken, config.Options)
	w.writer = w.client.WriteAPI(config.Org, config.Bucket)
	runtime.SetFinalizer(w, func(obj *Writer) {
		obj.client.Close()
	})
	return w
}

// Sync WriteSyncer sync implementation
func (w *Writer) Sync() error {
	w.writer.Flush()
	return nil
}

// Write WriteSyncer Write implementation
func (w *Writer) Write(p []byte) (int, error) {
	origin := len(p)
	if len(p) > 0 && p[len(p)-1] == '\n' {
		p = p[:len(p)-1]
	}
	point := influxdb2.NewPoint(w.config.Measurement, nil, map[string]interface{}{"log": p}, time.Now())
	w.writer.WritePoint(point)
	return origin, nil
}

// Health of influxdb
func (w *Writer) Health(t time.Time) (string, error) {
	ctx, cancel := context.WithDeadline(context.Background(), t)
	defer cancel()
	res, err := w.client.Health(ctx)
	if res != nil {
		return string(res.Status), err
	}
	return "", err
}

// Ping to influxdb
func (w *Writer) Ping() (bool, error) {
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(time.Second*3))
	defer cancel()
	return w.client.Ready(ctx)
}

// CheckError must be run in another goroutine
func (w *Writer) CheckError() {
	for {
		select {
		case err := <-w.writer.Errors():
			fmt.Println(err)
		}
	}
}
