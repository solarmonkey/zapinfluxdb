package zapinfluxdb

import (
	"context"
	"fmt"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"runtime"
	"time"
)

// Core zap core wrapper
type Core struct {
	client   influxdb2.Client
	writer   api.WriteAPI
	config   Config
	realCore zapcore.Core
}

// NewCore constructor
func NewCore(enc zapcore.Encoder, ws zapcore.WriteSyncer, enab zapcore.LevelEnabler, config Config) *Core {
	if config.Options == nil {
		config.Options = influxdb2.DefaultOptions()
	}
	c := &Core{
		config: config,
	}
	c.client = influxdb2.NewClientWithOptions(config.URL, config.AuthToken, config.Options)
	c.writer = c.client.WriteAPI(config.Org, config.Bucket)
	c.realCore = zapcore.NewCore(enc, ws, enab)
	runtime.SetFinalizer(c, func(obj *Core) {
		//fmt.Println("destructor")
		obj.client.Close()
	})
	return c
}

// With Core With implementation
func (cw *Core) With(fields []zap.Field) zapcore.Core {
	clone := &Core{}
	clone.realCore = cw.realCore.With(fields)
	clone.client = cw.client
	clone.config = cw.config
	clone.writer = cw.writer
	runtime.SetFinalizer(clone, func(obj *Core) {
		//fmt.Println("destructor")
		obj.client.Close()
	})
	return clone
}

// Enabled Core Enabled implementation
func (cw *Core) Enabled(level zapcore.Level) bool {
	return cw.realCore.Enabled(level)
}

// Check Core Check implementation
func (cw *Core) Check(e zapcore.Entry, ce *zapcore.CheckedEntry) *zapcore.CheckedEntry {
	if cw.Enabled(e.Level) {
		return ce.AddCore(e, cw)
	}
	return ce
}

// Write Core Write implementation
func (cw *Core) Write(e zapcore.Entry, fields []zapcore.Field) error {
	tagMap := make(map[string]string)
	tagMap[cw.config.LevelKey] = e.Level.String()
	tagMap[cw.config.CallerKey] = e.Caller.String()
	fieldMap := make(map[string]interface{})
	for _, f := range fields {
		if f.Interface != nil {
			fieldMap[f.Key] = f.Interface
		} else if len(f.String) != 0 {
			fieldMap[f.Key] = f.String
		} else {
			fieldMap[f.Key] = f.Integer
		}
	}
	fieldMap[cw.config.MessageKey] = e.Message
	point := influxdb2.NewPoint(cw.config.Measurement, tagMap, fieldMap, e.Time)
	cw.writer.WritePoint(point)
	return cw.realCore.Write(e, fields)
}

// Sync Core Sync implementation
func (cw *Core) Sync() error {
	cw.writer.Flush()
	return cw.realCore.Sync()
}

// Health of influxdb
func (cw *Core) Health(t time.Time) (string, error) {
	ctx, cancel := context.WithDeadline(context.Background(), t)
	defer cancel()
	res, err := cw.client.Health(ctx)
	if res != nil {
		return string(res.Status), err
	}
	return "", err
}

// Ping to influxdb
func (cw *Core) Ping() (bool, error) {
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(time.Second*3))
	defer cancel()
	return cw.client.Ready(ctx)
}

// CheckError must be run in another goroutine
func (cw *Core) CheckError() {
	for {
		select {
		case err := <-cw.writer.Errors():
			fmt.Println(err)
		}
	}
}
